

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.EncoderMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Stopwatch;

public class GyroControl {
	//ku=3.2
	//pu=0.28
	private static float KP = 1.8835f;
	private static float KI = 0.1355f;
	private static float KD = 0.06775f;

	private static final int SensorSampleCalibrateSize = 2;
	private static final int SensorSampleSize = 3;

	private EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S1);
	private EncoderMotor leftMotor = new UnregulatedMotor(MotorPort.D);
	private EncoderMotor rightMotor = new UnregulatedMotor(MotorPort.A);

	private static final float dt = 0.01f;
	private static final float angleScalingFactor = 20.0f;

	private float mean = 0;
	private float integralError = 0;
	private float previousError = 0;

	private Stopwatch stopwatch = new Stopwatch();

	private Thread balancer = null;

	private float[] dataError = new float[10000];
	private float[] power = new float[10000];
	private int it = 0;

	public static void main(String[] args) {
		GyroControl gyroBoy = new GyroControl();
		/*for (int i = 1; i < 10; i++) {
			KP = 3 + (i / 10f);
			LCD.drawString("Kp:" + KP, 0, 0);
			Button.ENTER.waitForPressAndRelease();
			gyroBoy.initialize();
			gyroBoy.start();
		}*/
		//while(true){
			gyroBoy.initialize();
			gyroBoy.start();	
	//	}
		//gyroBoy.gyroSensor.close();
	}

	public void initialize() {
		mean = calibrate();
		stopwatch.reset();
	}

	private void start() {
		balancer = new Thread(new Balancer());
		balancer.start();
		try {
			balancer.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private float calibrate() {
		Sound.playTone(440, 100, 10);

		fastDelay(100);

		mean = 0;
		gyroSensor.reset();

		for (int i = 0; i < SensorSampleCalibrateSize; i++) {
			mean = mean + gyroAngle();

			fastDelay(5);
		}

		mean = mean / SensorSampleCalibrateSize;

		fastDelay(100);

		Sound.playTone(440, 100, 10);

		fastDelay(100);

		Sound.playTone(440, 100, 10);

		return mean;
	}

	private float gyroRate() {
		float filter = 0;

		for (int i = 0; i < SensorSampleSize; i++) {
			filter = filter + getGyroSensorRateValue();
		}

		filter = filter / SensorSampleSize;

		return filter;
	}

	private float gyroAngle() {
		float filter = 0;

		for (int i = 0; i < SensorSampleSize; i++) {
			filter = filter + getGyroAngle();
		}

		filter = filter / SensorSampleSize;

		return filter;
	}

	private float getGyroAngle() {
		SampleProvider sampleProvider = gyroSensor.getAngleMode();
		if (sampleProvider.sampleSize() > 0) {
			float[] samples = new float[sampleProvider.sampleSize()];
			sampleProvider.fetchSample(samples, 0);
			// prevGyroAngle = samples[0];
			return samples[0];
		}
		return 0;
		// LCD.drawString("prevGyroAngle", 0, 1);
		// return prevGyroAngle;
	}

	private float getGyroSensorRateValue() {
		SampleProvider sampleProvider = gyroSensor.getRateMode();
		if (sampleProvider.sampleSize() > 0) {
			float[] samples = new float[sampleProvider.sampleSize()];
			sampleProvider.fetchSample(samples, 0);
			return samples[0];
		}
		return 0.0f;
	}

	public class Balancer implements Runnable {

		@Override
		public void run() {
			it = 0;
			while (true) {
				balanceLoop();
				it++;
				if (it >= 10000) {
					it--;
				}
				if (Math.abs(getGyroAngle()) > 60) {
					// The error is huge stop the motors
					leftMotor.setPower(0);
					rightMotor.setPower(0);
					break;
				}
			}
			// iterationComplete = true;
			// write data on a file to plot errors
			try {
				PrintWriter pw = new PrintWriter("data_" + KP + ".csv");
				for (int i = 0; i < it; i++) {
					pw.write(dataError[i] + ";" + power[i] + "\n");
				}
				pw.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void balanceLoop() {
		float gyroValue = gyroRate(); // do not need to calculate error since target is always zero
		float gyroAngle = (gyroAngle()-mean)*angleScalingFactor;
		
		// To keep the robot steady, we try to keep robot at zero angle speed and balanced gyro angle.

		float pidValue = pid(KP, KI, KD, dt, 0.0f, gyroValue + gyroAngle);

		setMotorPower(pidValue);
		dataError[it] = gyroValue;
		power[it] = pidValue;

		waitBalanceLoop();
	}

	private float pid(float kp, float ki, float kd, float dt,
			float referenceValue, float inputValue) {

		float currentError = inputValue - referenceValue;

		integralError = integralError + currentError * dt;

		float differentError = (currentError - previousError) / dt;
		previousError = currentError;

		return currentError * kp + integralError * ki + differentError * kd;
	}

	private void setMotorPower(float pidValue) {
		rightMotor.setPower((int) -pidValue);
		leftMotor.setPower((int) -pidValue);
	}

	private void waitBalanceLoop() {
		while ((stopwatch.elapsed() / 1000f) < dt) {
			fastDelay(1);
		}
		stopwatch.reset();
	}

	private void fastDelay(int millisecond) {
		try {
			Thread.sleep(millisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
